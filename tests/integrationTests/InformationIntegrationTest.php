<?php

use PHPUnit\Framework\TestCase;
require_once __DIR__."../../../src/contact/function.php";
/**
 * @covers \Information
 */
class InformationIntegrationTest extends TestCase
{
    private Information $information;

    public function __construct()
    {
        parent::__construct();
        $this->information = new Information();
    }

    public function testInsert(){
        $rep = $this->information->getAll();
        $this->information->insert("jb", "bertrand");
        $rap = $this->information->getAll();
        self::assertEquals(count($rep), count($rap)-1);
    }

    public function testSelect(){
        $this->information->insert("tonys", "parkers");
        $rep = $this->information->getAll();
//        $lastElement = end($rep);
//        $id = $lastElement['id'];
        $rap = $this->information->select($rep[count($rep)-1]['id']);

        self::assertEquals("tonys", $rap['name']);
        self::assertEquals("parkers", $rap['lastname']);
    }

    public function testUpdate(){
        $this->information->insert("john", "scoot");
        $rep = $this->information->getAll();

        $this->information->update(end($rep)['id'], "john", 'le rouge');
        $rap = $this->information->select(end($rep)['id']);

        self::assertEquals("john", $rap['name']);
        self::assertEquals("le rouge", $rap['lastname']);
    }

    public function testIntegrationGetAll(){
        $rep = $this->information->getAll();
        $this->information->insert("john", "scoot");
        $rap = $this->information->getAll();

        self::assertEquals(count($rap), count($rep) +1);
    }

    public function testDelete(){
        $rep = $this->information->getAll();
        $this->information->delete(end($rep)['id']);
        $rap = $this->information->getAll();

        self::assertEquals(count($rep)-1, count($rap));
    }
}

