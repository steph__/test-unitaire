<?php

use PHPUnit\Framework\TestCase;
require_once __DIR__."../../../src/contact/function.php";
/**
 * @covers \Information
 */
class InformationUnitTest extends TestCase
{
    private Information $information;

    public function __construct()
    {
        parent::__construct();
        $this->information = new Information();
    }

    public function testInsertWithoutname() {
        $this->expectException(PDOException::class);
        $this->information->insert(null, "tamo");
    }

    public function testInsertWithoutlastname() {
        $this->expectException(PDOException::class);
        $this->information->insert("jorge", null);
    }

    public function testInsertWithAllArguments() {
        $rep =  $this->information->insert("toma", "kean");
        self::assertTrue($rep);
    }

    public function testSelectWithInvalidId() {
        $rep = $this->information->select(null);
        self::assertFalse($rep);
    }

    public function testSelectWithValidId() {
        $rep = $this->information->select(1);
        self::assertEquals("mota", $rep['lastname']);
        self::assertEquals("franck", $rep['name']);
    }

    public function testUpdateWithoutName() {
        $this->expectException(PDOException::class);
        $this->information->update(2, null, "toto");
    }

    public function testUpdateWithoutLastname() {
        $this->expectException(PDOException::class);
        $this->information->update(2, 'tata', null);
    }

    public function testUpdateWithoutId() {
        $rep = $this->information->update(null, 'tata', 'tonton');
        self::assertTrue($rep);
    }

    public function testUpdateWithAllArguments() {
        $this->information->update(2, 'toto', 'tata');
        $rep = $this->information->select(2);
        self::assertEquals('tata',$rep['lastname']);
        self::assertEquals('toto',$rep['name']);

    }

    public function testDeleteWithInvalidId() {
        $rep = $this->information->delete(null);
        self::assertTrue($rep);
    }

    public function testDeleteWithvalidId() {
        $rep = $this->information->delete(3);
        self::assertTrue($rep);
    }

    public function testGetAll() {
        $rep = $this->information->getAll();
        self::assertIsArray($rep);
        self::assertNotEmpty($rep);
    }


}

