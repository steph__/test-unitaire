<?php

use PHPUnit\Framework\TestCase;
require_once __DIR__."../../../src/calculet/calcul.php";
/**
 * @covers \Calcul
 */
class CalculTest extends TestCase
    {
        public function testAdd()
        {
            $calc = new Calcul();
            $this->assertEquals(5, $calc->add(2, 3));
        }
        public function testAddN1IsNull()
        {
            $calc = new Calcul();
            $this->expectException(Exception::class);
            $this->expectExceptionMessage("N1 ne dois pas etre null!! remplacer N1!");
            $calc->add(null, 5);
        }
        public function testAddN2IsNull()
        {
            $calc = new Calcul();
            $this->expectException(Exception::class);
            $this->expectExceptionMessage("N2 ne dois pas etre null!! remplacer N2!");
            $calc->add(5, null);
        }
        public function testSou()
        {
            $calc = new Calcul();
            $this->assertEquals(5, $calc->sou(8, 3));
        }
        public function testMul()
        {
            $calc = new Calcul();
            $this->assertEquals(6, $calc->Mul(2, 3));
        }
        public function testDivN2IsNull()
        {
            $calc = new Calcul();
            $this->expectException(Exception::class);
            $this->expectExceptionMessage("impossible de diviser par 0 !");
            $calc->div(15, 0);
        }
        public function testDiv()
        {
            $calc = new Calcul();
            $this->assertEquals(5, $calc->div(15, 3));
        }
    }

