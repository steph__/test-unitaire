<?php
class Information
{
    private $pdo;
    public function __construct()
    {
        $this->pdo  = new PDO('sqlite:'.__DIR__.'../../../contact.sqlite');
    }
    public function insert($firstname, $lastname, $id=null)
    {
        $sql = "INSERT INTO contact(name, lastname, id) values(?, ?, ?)";
        $q = $this->pdo->prepare($sql);
        return $q->execute(array($firstname, $lastname, $id));
    }
    public function select($id)
    {
        $sql = "SELECT * FROM contact where id =?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($id));
        return $q->fetch(PDO::FETCH_ASSOC);
    }
    public function update($id, $name, $lastname)
    {
        $sql = "UPDATE contact SET name =?, lastname =? WHERE  id=?";
        $q = $this->pdo->prepare($sql);
        return $q->execute(array($name, $lastname, $id));
    }
    public function delete($id)
    {
        $sql = "DELETE FROM contact  WHERE id =?";
        $q = $this->pdo->prepare($sql);
        return $q->execute(array($id));
    }

    public function getAll()
    {
        $sql = "SELECT * FROM contact";
        $q = $this->pdo->prepare($sql);
        $q->execute();
        return $q->fetchAll(PDO::FETCH_ASSOC);
    }
}
